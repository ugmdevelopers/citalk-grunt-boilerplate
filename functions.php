<?php
/**
 * Citalk WP-Theme functions and definitions
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * @package Citalk WP-Theme
 * @since 0.1.0
 */
 
 // Useful global constants
define( 'CITALK__VERSION', '0.1.0' );
 
 /**
  * Set up theme defaults and register supported WordPress features.
  *
  * @uses load_theme_textdomain() For translation/localization support.
  *
  * @since 0.1.0
  */
 function citalk__setup() {
	/**
	 * Makes Citalk WP-Theme available for translation.
	 *
	 * Translations can be added to the /lang directory.
	 * If you're building a theme based on Citalk WP-Theme, use a find and replace
	 * to change 'citalk_' to the name of your theme in all template files.
	 */
	load_theme_textdomain( 'citalk_', get_template_directory() . '/languages' );
 }
 add_action( 'after_setup_theme', 'citalk__setup' );
 
 /**
  * Enqueue scripts and styles for front-end.
  *
  * @since 0.1.0
  */
 function citalk__scripts_styles() {
	$postfix = ( defined( 'SCRIPT_DEBUG' ) && true === SCRIPT_DEBUG ) ? '' : '.min';

	wp_enqueue_script( 'citalk_', get_template_directory_uri() . "/assets/js/citalk_wp_theme{$postfix}.js", array(), CITALK__VERSION, true );
		
	wp_enqueue_style( 'citalk_', get_template_directory_uri() . "/assets/css/citalk_wp_theme{$postfix}.css", array(), CITALK__VERSION );
 }
 add_action( 'wp_enqueue_scripts', 'citalk__scripts_styles' );
 
 /**
  * Add humans.txt to the <head> element.
  */
 function citalk__header_meta() {
	$humans = '<link type="text/plain" rel="author" href="' . get_template_directory_uri() . '/humans.txt" />';
	
	echo apply_filters( 'citalk__humans', $humans );
 }
 add_action( 'wp_head', 'citalk__header_meta' );